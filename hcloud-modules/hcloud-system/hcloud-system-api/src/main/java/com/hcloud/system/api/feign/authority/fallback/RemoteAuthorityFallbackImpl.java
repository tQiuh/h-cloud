package com.hcloud.system.api.feign.authority.fallback;

import com.hcloud.system.api.feign.authority.RemoteAuthorityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.hcloud.common.core.base.HCloudResult;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Slf4j
@AllArgsConstructor
public class RemoteAuthorityFallbackImpl implements RemoteAuthorityService {
    private final Throwable cause;
    @Override
    public HCloudResult<List<String>> findByRoleIds(String roleIds) {
        log.error("feign 通过roleId查询权限失败{}",roleIds,cause);
        return null;
    }
}
