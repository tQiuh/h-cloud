package com.hcloud.common.core.constants;

/**
 * @Auther hepangui
 * @Date 2018/11/29
 */
public interface OperateType {
    String ADD = "add";
    String UPDATE = "update";
    String DEL = "del";
    String QUERY ="query";
}
